from django import forms

from .models import Captcha

# TODO: Remove this Field, it is unused
class CaptchaField(forms.CharField):
	def __init__(self, min_length = 4, max_length = 4, *args, **kwargs):
		super(CaptchaField, self).__init__(min_length = min_length, max_length = max_length, *args, **kwargs)

	def clean(self, value):
		super(CaptchaField, self).clean()
		captcha_model = Captcha.objects.get(pk=id)
