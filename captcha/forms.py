from django import forms

from random import randint

from PIL import Image, ImageDraw, ImageFont
from io import BytesIO
import base64

from .models import Captcha

class CaptchaForm(forms.Form):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		if not self.is_bound:
			self.generate_captcha()

	id = forms.IntegerField(label="", widget=forms.HiddenInput(attrs = {"readonly": ""}))
	answer = forms.IntegerField(label="", widget=forms.TextInput(attrs = {"placeholder": "Enter Captcha Value"}))

	def clean(self):
		cleaned_data = super().clean()

		captcha_id = cleaned_data["id"]
		user_answer = cleaned_data["answer"]

		try:
			captcha = Captcha.objects.get(pk=captcha_id)
		except:
			self.regenerate_captcha()
			raise forms.ValidationError("Captcha Expired")
		if user_answer != captcha.answer:
			captcha.delete()
			self.regenerate_captcha() # Generate a new captcha if user submitted incorrect answer
			raise forms.ValidationError("Incorrect Captcha Value")
		captcha.delete()
		return cleaned_data

	# Generate Captcha for Bound Forms
	def regenerate_captcha(self):
		self.generate_captcha()

		data = self.data.copy()
		data["id"] = self.captcha.id
		data["answer"] = ""
		self.data = data

	def generate_captcha(self):
		question = randint(1000, 9999)
		self.captcha = Captcha.objects.create(answer = question)
		self.initial["id"] = self.captcha.id

		self.image_data = self.generate_image(str(question))

	def generate_image(self, text, w = 512, h = 256, thickness = 5):
		image = Image.new(mode = "1", size = (w, h), color = 1)
	
		imageDraw = ImageDraw.Draw(image)
		font = ImageFont.truetype("Gidole-Regular.ttf", w // 4)
		imageDraw.text((randint(w // 7, w // 4), randint(h // 9, h // 5)), text, 0, font=font)
	
		imageDraw.line(xy = [(0, randint(0, h // 4)), (w, randint(h - (h // 4), h))], fill = 0, width = thickness)
		imageDraw.line(xy = [(0, randint(h - (h // 4), h)), (w, randint(0, h // 4))], fill = 0, width = thickness)
		i = 0
		while i < 512:
			imageDraw.line(xy = [(0, i), (w, i)], fill = 0, width = thickness)
			imageDraw.line(xy = [(i, 0), (i, h)], fill = 0, width = thickness)
			i += randint(w // 20, w // 10)
		
		output = BytesIO()
		image.save(output, format="PNG")
		bytes = base64.b64encode(output.getvalue())
		return bytes.decode("utf-8")
