from django.db import models
from django.utils import timezone

from datetime import timedelta

class Captcha(models.Model):
	answer = models.IntegerField()

	def default_expiration():
		return timezone.now() + timedelta(days=1)

	expiration = models.DateTimeField(default=default_expiration)

