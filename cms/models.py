from django.db import models
from django.urls import reverse

class Post(models.Model):
	title = models.CharField(max_length=128)
	slug = models.SlugField(unique=True)
	published = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	content = models.TextField()

	def get_absolute_url(self):
		return reverse("cms-post", kwargs={"post_slug": self.slug})	

	def __str__(self):
		return self.title
