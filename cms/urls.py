from django.urls import path, include

from .views import PostView, PostListView

urlpatterns = [
	path("", PostListView.as_view(), name="cms-post-list"),
	path("<str:post_slug>/", PostView.as_view(), name="cms-post"),
]
