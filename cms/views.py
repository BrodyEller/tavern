from django.shortcuts import render, get_object_or_404
from django.views import View
from django.core.paginator import Paginator

from .models import Post

class PostView(View):
	template_name = "cms/post.html"

	def get(self, request, post_slug):
		data = {}
		data["post"] = get_object_or_404(Post, slug=post_slug, published=True)
		return render(request, self.template_name, data)

class PostListView(View):
	template_name = "cms/post_list.html"

	def get(self, request):
		posts = Post.objects.all().filter(published=True)
		paginator = Paginator(posts, 50)
		page_num = request.GET.get("page")
		current_page = paginator.get_page(page_num)
		data = {'posts': current_page}
		return render(request, self.template_name, data)
