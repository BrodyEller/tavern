from django.shortcuts import render

from posts.models import Tavern

def index(request):
	data = {}
	data["tavern"] = Tavern.objects.get(pk=1)
	return render(request, "home/index.html", data)
