from django.contrib import admin

from .models import Tavern, Post, Comment

@admin.register(Tavern)
class TavernAdmin(admin.ModelAdmin):
	list_display = ["name", "id"]
	search_fields = ["id", "name"]
	readonly_fields = ["id"]

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
	list_display = ["title", "tavern", "author", "archived", "id"]
	search_fields = ["id", "title", "author", "content"]
	readonly_fields = ["id"]

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
	list_display = ["content", "post", "author", "id"]
	search_fields = ["id", "author", "content"]
	readonly_fields = ["id"]
