from django import forms
from django.forms import Textarea

from .models import Comment, Post

class SearchForm(forms.Form):
	search = forms.CharField(label="", max_length=100)

class CommentForm(forms.ModelForm):
	class Meta:
		model = Comment
		fields = ["content"]
		widgets = {
			"content": Textarea(attrs={"cols": 32, "rows": 4}),
		}
		labels = {
			"content": ""
		}

class PostCreateForm(forms.ModelForm):
	class Meta:
		model = Post
		fields = ["title", "content"]
		widgets = {
			"content": Textarea(attrs={"cols": 32, "rows": 4}),
		}
