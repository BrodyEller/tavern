from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from datetime import timedelta

from posts.models import Post

class Command(BaseCommand):
	help = "Archive posts that were created more than 24 hours ago"

	def handle(self, *args, **kwargs):
		expired = Post.objects.filter(created_at__lt=(timezone.now() - timedelta(days=1))).filter(archived=False).update(archived=True)
		self.stdout.write("Marked " + str(expired) + " posts as expired")
