from django.db import models
from django.urls import reverse

class Tavern(models.Model):
	name = models.CharField(max_length=150)
	description = models.TextField(max_length=1024)
	# TODO: Add image field where image is hosted on separate domain. See https://docs.djangoproject.com/en/3.1/topics/security/#user-uploaded-content-security
	image = models.ImageField(upload_to="images/")
	latitude = models.FloatField()
	longitude = models.FloatField()

	def get_absolute_url(self):
		return reverse("posts-tavern", kwargs={"tavern_id": self.id})

	def __str__(self):
		return self.name

class Post(models.Model):
	tavern = models.ForeignKey("Tavern", on_delete=models.CASCADE)
	title = models.CharField(max_length=128)
	author = models.CharField(max_length=8, default="", blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	archived = models.BooleanField(default=False)
	content = models.TextField(max_length=4096)

	def get_absolute_url(self):
		return reverse("posts-post", kwargs={"tavern_id": self.tavern.id, "post_id": self.id})	

	def __str__(self):
		return self.title

class Comment(models.Model):
	post = models.ForeignKey("Post", on_delete=models.CASCADE)
	author = models.CharField(max_length=8)
	created_at = models.DateTimeField(auto_now_add=True)
	content = models.CharField(max_length=512)
	
	def get_absolute_url(self):
		return reverse("posts-post", kwargs={"tavern_id": self.post.tavern.id, "post_id": self.post.id})	

	def __str__(self):
		return self.content
