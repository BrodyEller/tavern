from .models import Tavern, Post, Comment
from rest_framework import serializers

class TavernSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Tavern
		fields = ["name", "latitude", "longitude"]

class PostSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Post
		fields = ["tavern", "author", "created_at", "content"]

class CommentSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Comment
		fields = ["post", "author", "created_at", "content"]
