from django.urls import path, include
from rest_framework import routers

from .views import TavernViewSet, PostViewSet, CommentViewSet
from .views import TavernListView, TavernView, PostView, PostCreateView

router = routers.DefaultRouter()
router.register(r"taverns", TavernViewSet)
router.register(r"posts", PostViewSet)
router.register(r"comments", CommentViewSet)


urlpatterns = [
	path("", TavernListView.as_view(), name="posts-tavern_list"),	
	path("<int:tavern_id>/", TavernView.as_view(), name="posts-tavern"),
	path("<int:tavern_id>/<int:post_id>/", PostView.as_view(), name="posts-post"),
	path("<int:tavern_id>/new/", PostCreateView.as_view(), name="posts-post_create"),

	# Rest API
	path("api/", include(router.urls)),
	path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
]
