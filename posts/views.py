from django.shortcuts import render
from django.views import View
from django.shortcuts import get_object_or_404, redirect
from django.core.paginator import Paginator
from django.core.exceptions import PermissionDenied

from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser
from rest_framework import mixins

from rest_framework_api_key.permissions import HasAPIKey

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter

from .serializers import TavernSerializer, PostSerializer, CommentSerializer
from .models import Tavern, Post, Comment

from .forms import SearchForm, CommentForm, PostCreateForm
from captcha.forms import CaptchaForm

class TavernListView(View):
	search_form_class = SearchForm
	template_name = "posts/tavern_list.html"

	def get(self, request):
		if request.GET.get("search"):
			search_form = self.search_form_class(request.GET)
			if search_form.is_valid():
				taverns = Tavern.objects.filter(name__contains=search_form.cleaned_data["search"])
		else:
			search_form = self.search_form_class()
			taverns = Tavern.objects.all()

		paginator = Paginator(taverns, 50)
		page_num = request.GET.get("page")
		current_page = paginator.get_page(page_num)
		data = {'taverns': current_page, "search_form": search_form}
		return render(request, self.template_name, data)


class TavernView(View):
	template_name = "posts/tavern.html"

	def get(self, request, tavern_id):
		posts = Post.objects.filter(tavern_id=tavern_id).order_by("-created_at")
		paginator = Paginator(posts, 50)
		page_num = request.GET.get("page")
		current_page = paginator.get_page(page_num)

		data = {}
		data["tavern"] = get_object_or_404(Tavern, pk=tavern_id)
		data["posts"] = current_page
		return render(request, self.template_name, data)


class PostView(View):
	template_name = "posts/post.html"

	def get(self, request, tavern_id, post_id):
		data = {}
		data["tavern"] = get_object_or_404(Tavern, pk=tavern_id)
		data["post"] = get_object_or_404(Post, pk=post_id)
		data["comments"] = Comment.objects.filter(post_id=post_id).order_by("created_at")
		data["comment_form"] = CommentForm()
		data["captcha_form"] = CaptchaForm()
		return render(request, self.template_name, data)

	def post(self, request, tavern_id, post_id):
		post = get_object_or_404(Post, pk=post_id)
		if post.archived:
			raise PermissionDenied

		form = CommentForm(request.POST)
		captcha_form = CaptchaForm(request.POST)
		if form.is_valid() and captcha_form.is_valid():
			comment = Comment.objects.create(post_id=post_id, content=form.cleaned_data["content"])
			return redirect(comment)
		data = {}
		data["tavern"] = get_object_or_404(Tavern, pk=tavern_id)
		data["post"] = post
		data["comments"] = Comment.objects.filter(post_id=post_id).order_by("created_at")
		data["comment_form"] = form
		data["captcha_form"] = captcha_form
		return render(request, self.template_name, data)

class PostCreateView(View):
	template_name = "posts/post_create.html"

	def get(self, request, tavern_id):
		data = {}
		data["tavern"] = get_object_or_404(Tavern, pk=tavern_id)
		data["post_create_form"] = PostCreateForm()
		data["captcha_form"] = CaptchaForm()
		return render(request, self.template_name, data)
	
	def post(self, request, tavern_id):
		form = PostCreateForm(request.POST)
		captcha_form = CaptchaForm(request.POST)
		if form.is_valid() and captcha_form.is_valid():
			post = Post.objects.create(tavern_id=tavern_id, title=form.cleaned_data["title"], content=form.cleaned_data["content"])
			return redirect(post)
		data = {}
		data["tavern"] = get_object_or_404(Tavern, pk=tavern_id)
		data["post_create_form"] = form
		data["captcha_form"] = captcha_form
		return render(request, self.template_name, data)

# Rest API

class CreateListRetrieveViewSet(mixins.CreateModelMixin, 
				mixins.ListModelMixin, 
				mixins.RetrieveModelMixin, 
				viewsets.GenericViewSet):
	pass


class TavernViewSet(viewsets.ReadOnlyModelViewSet):
	queryset = Tavern.objects.all()
	serializer_class = TavernSerializer
	permission_classes = [IsAdminUser | HasAPIKey]
	filter_backends = [SearchFilter]
	search_fields = ["name"]

class PostViewSet(CreateListRetrieveViewSet):
	queryset = Post.objects.all()
	serializer_class = PostSerializer
	permission_classes = [IsAdminUser | HasAPIKey]
	filter_backends = [DjangoFilterBackend]
	filterset_fields = ["tavern", "author"]

class CommentViewSet(CreateListRetrieveViewSet):
	queryset = Comment.objects.all()
	serializer_class = CommentSerializer
	permission_classes = [IsAdminUser | HasAPIKey]

